const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave:false,
  devServer: {
    proxy: {
      '/api': {
        target: 'http://192.168.10.8:8000',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/api': ''
          }
        }
      }
    }
})

