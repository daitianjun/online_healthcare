import { Vue,createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import ElementPlus from 'element-plus' 
import 'element-plus/dist/index.css'

createApp(App).use(VueAxios, axios).use(store).use(ElementPlus).use(router).mount('#app')
